import React from 'react'
import { createStore, TStore } from './store';
import { useLocalStore } from 'mobx-react-lite';

const Context = React.createContext<TStore | null>(null)

interface Props {
  children: React.ReactElement;
}

export const Provider: React.FC<Props> = (props) => {
  const store = useLocalStore(createStore);
  return <Context.Provider value={store}>{props.children}</Context.Provider>
}

export const useStore = () => React.useContext(Context);

export function withProvider(Component: any) {
  return function WrapperComponent(props: any) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}
