export type TCounter = {
  index: number;
  increment: Function;
  decrement: Function;
}

export function createStore() {
  return {
    index: 0,
    increment() {
      this.index++;
    },
    decrement() {
      this.index--;
    },
  } as TCounter
}

export type TStore = ReturnType<typeof createStore>