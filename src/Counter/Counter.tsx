import React from 'react';
import { useObserver } from 'mobx-react-lite';
import { withProvider, useStore } from '../context';

const Header = () => {
  console.log('Render 2');
  const store = useStore();
  return useObserver(() => <h1>{store?.index}</h1>);
};

const Counter = () => {
  const store = useStore();
  console.log('Render 1');
  return useObserver(() => <>
    <Header />
    <button onClick={() => { store?.increment() }}>+</button>
    <button onClick={() => { store?.decrement() }}>-</button>
  </>);
};

export default withProvider(Counter);
